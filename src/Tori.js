import React from "react";
import "./Tori.css";

const Tori = () => {
  return (
    <div className="tori">
      <div className="ylempi">
      <input className="hakusana" type="text" value="Hakusana ja/tai postinumero" />
      <select className="osasto" name="what">
        <option id="1">Kaikki osastot</option>
      </select>
      <select className="sijainti" name="where">
        <option id="1">Koko Suomi</option>
      </select>
      </div>
      <div className="napit">
      <input type="checkbox" name="how" />
      Myydään
      <input type="checkbox" name="how" />
      Ostetaan
      <input type="checkbox" name="how" />
      Vuokrataan
      <input type="checkbox" name="how" />
      Halutaan vuokrata
      <input type="checkbox" name="how" />
      Annetaan 
      </div>
      <div className="hakuvalikko">
      <a className="haku">Tallenna haku</a>
      <button className="hae">Hae</button>
      </div>
    </div>
  );
};

export default Tori;
